;
;　アセンブラルーチンからTCB構造体のメンバにアクセスするための
;　オフセット定義
;

; 注意
; 　いずれも0x80以上になると符号拡張され、
; 　期待した動作にならない。

TCB_p_tinib:		.DEFINE	"(8)"
TCB_texptn:			.DEFINE	"(H'14)"
TCB_sp:				.DEFINE	"(H'1c)"
TCB_pc:				.DEFINE	"(H'20)"
TINIB_exinf:		.DEFINE	"(4)"
TINIB_task:			.DEFINE	"(8)"

; バイトアクセス
TCB_enatex:			.DEFINE	"(H'11)"
TCB_enatex_bit: 	.DEFINE	"(5)"
;TCB_enatex_mask:	.DEFINE	"H'20"
TCB_enatex_mask:	.DEFINE	"(1 << TCB_enatex_bit)"

