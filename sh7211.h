/*
 *	TOPPERS/ASP Kernel
 *		Toyohashi Open Platform for Embedded Real-Time Systems/
 *		Advanced Standard Profile Kernel
 *	
 *	Copyright (C) 2007 by Embedded and Real-Time Systems Laboratory
 *				Graduate School of Information Science, Nagoya Univ., JAPAN
 *	Copyright (C) 2007-2010 by Industrial Technology Institute,
 *								Miyagi Prefectural Government, JAPAN
 *	
 *	上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *	ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *	変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *	(1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *		権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *		スコード中に含まれていること．
 *	(2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *		用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *		者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *		の無保証規定を掲載すること．
 *	(3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *		用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *		と．
 *	  (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *		  作権表示，この利用条件および下記の無保証規定を掲載すること．
 *	  (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *		  報告すること．
 *	(4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *		害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *		また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *		由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *		免責すること．
 *	
 *	本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *	よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *	に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *	アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *	の責任を負わない．
 *	
 *	$Id: sh7211.h 1824 2010-07-02 06:50:03Z mit-kimai $
 */

/*
 *	SH7211のハードウェア資源の定義
 */

#ifndef TOPPERS_SH7211_H
#define TOPPERS_SH7211_H

/*
 *	例外ベクタ
 */
#define POWER_ON_RESET_VECTOR				0
#define MANUAL_RESET_VECTOR					2
#define GENERAL_ILLEGAL_INSTRUCTION_VECTOR	4
#define RAM_ERROR_VECTOR					5
#define SLOT_ILLEGAL_INSTRUCTION_VECTOR		6
#define CPU_ADDRESS_ERROR_VECTOR			9
#define DMA_ADDRESS_ERROR_VECTOR			10
#define FPU_VECTOR							13
#define BANK_OVER_FLOW_VECTOR				15
#define BANK_UNDER_FLOW_VECTOR				16
#define DIVIDE_BY_ZERO_VECTOR				17

/*
 *	割込みベクタ
 */
#define IRQ0_VECTOR 	   			64
#define IRQ1_VECTOR 	   			65
#define IRQ2_VECTOR 	   			66
#define IRQ3_VECTOR 	   			67
#define IRQ4_VECTOR 	   			68
#define IRQ5_VECTOR 	   			69
#define IRQ6_VECTOR 	   			70
#define IRQ7_VECTOR    				71

#define ADI_VECTOR					92

#define DMAC0_TEI0_VECTOR		 	108
#define DMAC0_HEI0_VECTOR		 	109
#define DMAC1_TEI1_VECTOR		 	112
#define DMAC1_HEI1_VECTOR		 	113
#define DMAC2_TEI2_VECTOR		 	116
#define DMAC2_HEI2_VECTOR		 	117
#define DMAC3_TEI3_VECTOR		 	120
#define DMAC3_HEI3_VECTOR		 	121
#define DMAC4_TEI4_VECTOR		 	124
#define DMAC4_HEI4_VECTOR		 	125
#define DMAC5_TEI5_VECTOR		 	128
#define DMAC5_HEI5_VECTOR		 	129
#define DMAC6_TEI6_VECTOR		 	132
#define DMAC6_HEI6_VECTOR		 	133
#define DMAC7_TEI7_VECTOR		 	136
#define DMAC7_HEI7_VECTOR		 	137

#define CMI0_VECTOR 	   			140
#define CMI1_VECTOR 	   			144
#define CMI_VECTOR		  			148

#define ITI_VECTOR		  			152

#define MTU0_TGI0A_VECTOR		 	156
#define MTU0_TGI0B_VECTOR		 	157
#define MTU0_TGI0C_VECTOR		 	158
#define MTU0_TGI0D_VECTOR		 	159
#define MTU0_TGI0V_VECTOR		 	160
#define MTU0_TGI0E_VECTOR		 	161
#define MTU0_TGI0F_VECTOR		 	162

#define MTU1_TGI1A_VECTOR		 	164
#define MTU1_TGI1B_VECTOR		 	165
#define MTU1_TGI1V_VECTOR		 	168
#define MTU1_TGI1U_VECTOR		 	169

#define MTU2_TGI2A_VECTOR		 	172
#define MTU2_TGI2B_VECTOR		 	173
#define MTU2_TGI2V_VECTOR		 	176
#define MTU2_TGI2U_VECTOR		 	177

#define MTU3_TGI3A_VECTOR		 	180
#define MTU3_TGI3B_VECTOR		 	181
#define MTU3_TGI3C_VECTOR		 	182
#define MTU3_TGI3D_VECTOR		 	183
#define MTU3_TGI3V_VECTOR		 	184

#define MTU4_TGI4A_VECTOR		 	188
#define MTU4_TGI4B_VECTOR		 	189
#define MTU4_TGI4C_VECTOR		 	190
#define MTU4_TGI4D_VECTOR		 	191
#define MTU4_TGI4V_VECTOR		 	192

#define MTU5_TGI5U_VECTOR		 	196
#define MTU5_TGI5V_VECTOR		 	197
#define MTU5_TGI5W_VECTOR		 	198

#define OEI1_VECTOR 	   			200
#define OEI2_VECTOR 	   			201

#define MTU3S_TGI3A_VECTOR		  	204
#define MTU3S_TGI3B_VECTOR		  	205
#define MTU3S_TGI3C_VECTOR		  	206
#define MTU3S_TGI3D_VECTOR		  	207
#define MTU3S_TGI3V_VECTOR		  	208

#define MTU4S_TGI3A_VECTOR		  	212
#define MTU4S_TGI3B_VECTOR		  	213
#define MTU4S_TGI3C_VECTOR		  	214
#define MTU4S_TGI3D_VECTOR		  	215
#define MTU4S_TGI3V_VECTOR		  	216

#define MTU5S_TGI5U_VECTOR		  	220
#define MTU5S_TGI5V_VECTOR		  	221
#define MTU5S_TGI5W_VECTOR		  	222

#define OEI3_VECTOR 	   			224

#define IIC3_STPI_VECTOR			228
#define IIC3_NAKI_VECTOR			229
#define IIC3_RXI_VECTOR    			230
#define IIC3_TXI_VECTOR    			231
#define IIC3_TEI_VECTOR    			232

#define SCIF0_BRI_VECTOR   			240
#define SCIF0_ERI_VECTOR   			241
#define SCIF0_RXI_VECTOR   			242
#define SCIF0_TXI_VECTOR   			243

#define SCIF1_BRI_VECTOR   			244
#define SCIF1_ERI_VECTOR   			245
#define SCIF1_RXI_VECTOR   			246
#define SCIF1_TXI_VECTOR   			247

#define SCIF2_BRI_VECTOR   			248
#define SCIF2_ERI_VECTOR   			249
#define SCIF2_RXI_VECTOR   			250
#define SCIF2_TXI_VECTOR   			251

#define SCIF3_BRI_VECTOR   			252
#define SCIF3_ERI_VECTOR   			253
#define SCIF3_RXI_VECTOR   			254
#define SCIF3_TXI_VECTOR   			255

#define WAVEIF_ERR_VECTOR			256
#define WAVEIF_WRXI_VECTOR			257
#define WAVEIF_WTXI_VECTOR			258

/*
 * 割込みコントローラレジスタ（INTC）
 */
#define ICR0	UINT_C(0xfffe0800)
#define ICR1	UINT_C(0xfffe0802)
#define IRQRR	UINT_C(0xfffe0806)
#define IBCR	UINT_C(0xfffe080c)
#define IBNR	UINT_C(0xfffe080e)
 #define IBNR_BE0	 UINT_C(0x4000)
 #define IBNR_BOVE	 UINT_C(0x2000)

#define IPR01	UINT_C(0xfffe0818)
#define IPR02	UINT_C(0xfffe081a)
#define IPR05	UINT_C(0xfffe0820)
#define IPR06	UINT_C(0xfffe0c00)
#define IPR07	UINT_C(0xfffe0c02)
#define IPR08	UINT_C(0xfffe0c04)
#define IPR09	UINT_C(0xfffe0c06)
#define IPR10	UINT_C(0xfffe0c08)
#define IPR11	UINT_C(0xfffe0c0a)
#define IPR12	UINT_C(0xfffe0c0c)
#define IPR13	UINT_C(0xfffe0c0e)
#define IPR14	UINT_C(0xfffe0c10)
#define IPR15	UINT_C(0xfffe0c12)

#define IRQ_POSEDGE   UINT_C(0x02)
#define IRQ_NEGEDGE   UINT_C(0x01)

#define INTC_BASE	ICR0	/*	ベースアドレス	*/

/*
 *	コンペアマッチ・タイマ（MCT）
 */
#define CMSTR	  UINT_C(0XFFFEC000)
#define CMCSR_0   UINT_C(0XFFFEC002)
#define CMCNT_0   UINT_C(0XFFFEC004)
#define CMCOR_0   UINT_C(0XFFFEC006)

#define CMSTR_STR0	 UINT_C(0x0001)
#define CMCSR_CMF	 UINT_C(0x0080)
#define CMCSR_CMIE	 UINT_C(0x0040)

#define CMT_BASE	CMSTR	/*	ベースアドレス	*/

/*
 *	FIFO付きシリアルコミュニケーションインターフェース(SCIF)レジスタ
 */
#define SH_SCIF0_BASE	UINT_C(0xFFFE8000)	/*	ベースアドレス	*/
#define SH_SCIF1_BASE	UINT_C(0xFFFE8800)
#define SH_SCIF2_BASE	UINT_C(0xFFFE9000)

/*
 *	ピンファンクションコントローラ（PFC）
 */
#define PAIORH	 UINT_C(0xFFFE3804)
#define PAIORL	 UINT_C(0xFFFE3806)
#define PACRH3	 UINT_C(0xFFFE380A)

#define PACRL1	 UINT_C(0xfffe3816)
#define PACRL2	 UINT_C(0xfffe3814)
#define PACRL3	 UINT_C(0xfffe3812)
#define PACRL4	 UINT_C(0xfffe3810)

#define PBIORH	 UINT_C(0xfffe3884)
#define PBIORL	 UINT_C(0xfffe3886)
#define PBCRH1	 UINT_C(0xfffe388e)
#define PBCRL1	 UINT_C(0xfffe3896)
#define PBCRL2	 UINT_C(0xfffe3894)
#define PBCRL3	 UINT_C(0xfffe3892)

#define PDIOR	UINT_C(0xfffe3986)
#define PDCRL1	 UINT_C(0xfffe3996)
#define PDCRL2	 UINT_C(0xfffe3994)
#define PDCRL3	 UINT_C(0xfffe3992)
#define PDCRL4	 UINT_C(0xfffe3990)

#define PFC_BASE	PAIORH	/*	ベースアドレス	*/

/*
 *	クロックパルス発振器（CPG）
 */
#define FRQCR	UINT_C(0xfffe0010)

/*
 *	低消費電力モード関連（PDM：Power-Down Modes）
 */
#define STBCR	UINT_C(0xfffe0014)
#define STBCR2	UINT_C(0xfffe0018)
 #define STBCR2_HUDI	UINT_C(0x80)
 #define STBCR2_UCB		UINT_C(0x40)
 #define STBCR2_DMAC	UINT_C(0x20)
#define STBCR3	UINT_C(0xfffe0408)
 #define STBCR3_HZ		UINT_C(0x80)
 #define STBCR3_MTU2S	UINT_C(0x40)
 #define STBCR3_MTU2	UINT_C(0x20)
 #define STBCR3_POE2	UINT_C(0x10)
 #define STBCR3_IIC3	UINT_C(0x08)
 #define STBCR3_ADC		UINT_C(0x04)
 #define STBCR3_DAC		UINT_C(0x02)
 #define STBCR3_FLASH	UINT_C(0x01)
#define STBCR4	UINT_C(0xfffe040C)
 #define STBCR4_SCIF0	UINT_C(0x80)
 #define STBCR4_SCIF1	UINT_C(0x40)
 #define STBCR4_SCIF2	UINT_C(0x20)
 #define STBCR4_SCIF3	UINT_C(0x10)
 #define STBCR4_CMT		UINT_C(0x04)
 #define STBCR4_WAVEIF	UINT_C(0x02)

#define SYSCR1	UINT_C(0xfffe0402)
#define SYSCR2	UINT_C(0xfffe0404)

#define PDM_BASE	STBCR	/*	ベースアドレス	*/

/*
 *	バスステートコントローラ（BSC）
 */
#define CMNCR	UINT_C(0xfffc0000)
#define CS3BCR	UINT_C(0xfffc0010)
#define CS3WCR	UINT_C(0xfffc0034)
#define SDCR	UINT_C(0xfffc004c)
#define RTCSR	UINT_C(0xfffc0050)
#define RTCOR	UINT_C(0xfffc0058)

#define BSC_BASE	CMNCR	/*	ベースアドレス	*/

/*
 *	内蔵RAM
 */
#define INNER_RAM_PAGE1_START		UINT_C(0xfff80000)
#define INNER_RAM_PAGE2_START		UINT_C(0xfff82000)
#define INNER_RAM_PAGE3_START		UINT_C(0xfff84000)
#define INNER_RAM_PAGE4_START		UINT_C(0xfff86000)

#define INNER_RAM_PAGE_SIZE			UINT_C(0x00002000)

#define INNER_RAM_PAGE4_END		(INNER_RAM_PAGE4_START + INNER_RAM_PAGE_SIZE)
/*  ページ4以外は次のページの先頭アドレスを使えばよい。  */

#endif /* TOPPERS_SH7711_H */
