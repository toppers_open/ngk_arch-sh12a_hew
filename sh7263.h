/*
 *  TOPPERS/ASP Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Advanced Standard Profile Kernel
 * 
 *  Copyright (C) 2007 by Embedded and Real-Time Systems Laboratory
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2008 by Witz Corporation, JAPAN
 * 
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 * 
 */

/*
 *  SH7263のハードウェア資源の定義
 */

#ifndef TOPPERS_SH7263_H
#define TOPPERS_SH7263_H

/*
 *  例外ベクタ
 */
#define POWER_ON_RESET_VECTOR				0
#define MANUAL_RESET_VECTOR					2
#define GENERAL_ILLEGAL_INSTRUCTION_VECTOR	4
#define RAM_ERROR_VECTOR					5
#define SLOT_ILLEGAL_INSTRUCTION__VECTOR	6
#define CPU_ADDRESS_ERROR_VECTOR			9
#define DMA_ADDRESS_ERROR_VECTOR			10
#define FPU_VECTOR							13
#define BANK_OVER_FLOW_VECTOR				15
#define BANK_UNDER_FLOW_VECTOR				16
#define DIVIDE_BY_ZERO_VECTOR				17
#define OVER_FLOW_VECTOR					18

/*
 *  割込みベクタ
 */
#define IRQ0_VECTOR        			64
#define IRQ1_VECTOR        			65
#define IRQ2_VECTOR        			66
#define IRQ3_VECTOR        			67
#define IRQ4_VECTOR        			68
#define IRQ5_VECTOR        			69
#define IRQ6_VECTOR        			70
#define IRQ7_VECTOR    				71

#define PINT0_VECTOR				80
#define PINT1_VECTOR				81
#define PINT2_VECTOR				82
#define PINT3_VECTOR				83
#define PINT4_VECTOR				84
#define PINT5_VECTOR				85
#define PINT6_VECTOR				86
#define PINT7_VECTOR				87

#define DMAC0_TEI_VECTOR        	108
#define DMAC0_HEI_VECTOR        	109
#define DMAC1_TEI_VECTOR        	112
#define DMAC1_HEI_VECTOR        	113
#define DMAC2_TEI_VECTOR        	116
#define DMAC2_HEI_VECTOR        	117
#define DMAC3_TEI_VECTOR        	120
#define DMAC3_HEI_VECTOR        	121
#define DMAC4_TEI_VECTOR        	124
#define DMAC4_HEI_VECTOR        	125
#define DMAC5_TEI_VECTOR        	128
#define DMAC5_HEI_VECTOR        	129
#define DMAC6_TEI_VECTOR        	132
#define DMAC6_HEI_VECTOR        	133
#define DMAC7_TEI_VECTOR        	136
#define DMAC7_HEI_VECTOR        	137

#define USB_VECTOR					140
#define LCDC_VECTOR					141

#define CMI0_VECTOR        			142
#define CMI1_VECTOR        			143
#define CMI_VECTOR        			144

#define ITI_VECTOR        			145

#define MTU0_TGIA_VECTOR        	146
#define MTU0_TGIB_VECTOR        	147
#define MTU0_TGIC_VECTOR        	148
#define MTU0_TGID_VECTOR        	149
#define MTU0_TGIV_VECTOR        	150
#define MTU0_TGIE_VECTOR        	151
#define MTU0_TGIF_VECTOR        	152

#define MTU1_TGIA_VECTOR        	153
#define MTU1_TGIB_VECTOR        	154
#define MTU1_TGIV_VECTOR        	155
#define MTU1_TGIU_VECTOR        	156

#define MTU2_TGIA_VECTOR        	157
#define MTU2_TGIB_VECTOR        	158
#define MTU2_TGIV_VECTOR        	159
#define MTU2_TGIU_VECTOR        	160

#define MTU3_TGIA_VECTOR        	161
#define MTU3_TGIB_VECTOR        	162
#define MTU3_TGIC_VECTOR        	163
#define MTU3_TGID_VECTOR        	164
#define MTU3_TGIV_VECTOR        	165

#define MTU4_TGIA_VECTOR        	166
#define MTU4_TGIB_VECTOR        	167
#define MTU4_TGIC_VECTOR        	168
#define MTU4_TGID_VECTOR        	169
#define MTU4_TGIV_VECTOR        	170

#define ADI_VECTOR					171

#define IIC3_STPI0_VECTOR			172
#define IIC3_NAKI0_VECTOR			173
#define IIC3_RXI0_VECTOR			174
#define IIC3_TXI0_VECTOR			175
#define IIC3_TEI0_VECTOR			176

#define IIC3_STPI1_VECTOR			177
#define IIC3_NAKI1_VECTOR			178
#define IIC3_RXI1_VECTOR			179
#define IIC3_TXI1_VECTOR			180
#define IIC3_TEI1_VECTOR			181

#define IIC3_STPI2_VECTOR			182
#define IIC3_NAKI2_VECTOR			183
#define IIC3_RXI2_VECTOR			184
#define IIC3_TXI2_VECTOR			185
#define IIC3_TEI2_VECTOR			186

#define IIC3_STPI3_VECTOR			187
#define IIC3_NAKI3_VECTOR			188
#define IIC3_RXI3_VECTOR			189
#define IIC3_TXI3_VECTOR			190
#define IIC3_TEI3_VECTOR			191

#define SCIF0_BRI_VECTOR			192
#define SCIF0_ERI_VECTOR			193
#define SCIF0_RXI_VECTOR			194
#define SCIF0_TXI_VECTOR			195

#define SCIF1_BRI_VECTOR			196
#define SCIF1_ERI_VECTOR			197
#define SCIF1_RXI_VECTOR			198
#define SCIF1_TXI_VECTOR			199

#define SCIF2_BRI_VECTOR			200
#define SCIF2_ERI_VECTOR			201
#define SCIF2_RXI_VECTOR			202
#define SCIF2_TXI_VECTOR			203

#define SCIF3_BRI_VECTOR			204
#define SCIF3_ERI_VECTOR			205
#define SCIF3_RXI_VECTOR			206
#define SCIF3_TXI_VECTOR			207

#define SSU0_SSERI_VECTOR			208
#define SSU0_SSRXI_VECTOR			209
#define SSU0_SSTXI_VECTOR			210

#define SSU1_SSERI_VECTOR			211
#define SSU1_SSRXI_VECTOR			212
#define SSU1_SSTXI_VECTOR			213

#define SSI0_SSII_VECTOR			214
#define SSI1_SSII_VECTOR			215
#define SSI2_SSII_VECTOR			216
#define SSI3_SSII_VECTOR			217

#define ROM_DEC_ISY_VECTOR			218
#define ROM_DEC_IERR_VECTOR			219
#define ROM_DEC_IARG_VECTOR			220
#define ROM_DEC_ISEC_VECTOR			221
#define ROM_DEC_IBUF_VECTOR			222
#define ROM_DEC_IREADY_VECTOR		223

#define FLCTL_FLSTEI_VECTOR			224
#define FLCTL_FLTENDI_VECTOR		225
#define FLCTL_FLTREQ0I_VECTOR		226
#define FLCTL_FLTREQ1I_VECTOR		227

#define SDHI_SDHI3_VECTOR			228
#define SDHI_SDHI0_VECTOR			229
#define SDHI_SDHI1_VECTOR			230

#define RTC_ARM_VECTOR				231
#define RTC_PRD_VECTOR				232
#define RTC_CUP_VECTOR				233

#define RCAN_TL1_RCAN0_ERS_VECTOR	234
#define RCAN_TL1_RCAN0_OVR_VECTOR	235
#define RCAN_TL1_RCAN0_RM0_VECTOR	236
#define RCAN_TL1_RCAN0_RM1_VECTOR	237
#define RCAN_TL1_RCAN0_SLE_VECTOR	238

#define RCAN_TL1_RCAN1_ERS_VECTOR	239
#define RCAN_TL1_RCAN1_OVR_VECTOR	240
#define RCAN_TL1_RCAN1_RM0_VECTOR	241
#define RCAN_TL1_RCAN1_RM1_VECTOR	242
#define RCAN_TL1_RCAN1_SLE_VECTOR	243

#define SRC_OVF_VECTOR				244
#define SRC_ODFI_VECTOR				245
#define SRC_IDEI_VECTOR				246

#define IEB_IEBI_VECTOR				247

/*
 * 割り込みコントローラレジスタ
 */
#define ICR0    UINT_C(0xfffe0800)
#define ICR1    UINT_C(0xfffe0802)
#define ICR2    UINT_C(0xfffe0804)
#define IRQRR   UINT_C(0xfffe0806)
#define PINTER  UINT_C(0xfffe0808)
#define PIRR    UINT_C(0xfffe080a)
#define IBCR    UINT_C(0xfffe080c)
#define IBNR    UINT_C(0xfffe080e)

#define IPR01   UINT_C(0xfffe0818)
#define IPR02   UINT_C(0xfffe081a)
#define IPR05   UINT_C(0xfffe0820)
#define IPR06   UINT_C(0xfffe0c00)
#define IPR07   UINT_C(0xfffe0c02)
#define IPR08   UINT_C(0xfffe0c04)
#define IPR09   UINT_C(0xfffe0c06)
#define IPR10   UINT_C(0xfffe0c08)
#define IPR11   UINT_C(0xfffe0c0a)
#define IPR12   UINT_C(0xfffe0c0c)
#define IPR13   UINT_C(0xfffe0c0e)
#define IPR14   UINT_C(0xfffe0c10)
#define IPR15   UINT_C(0xfffe0c12)
#define IPR16   UINT_C(0xfffe0c14)
#define IPR17   UINT_C(0xfffe0c16)

#define IRQ_POSEDGE   UINT_C(0x02)
#define IRQ_NEGEDGE   UINT_C(0x01)

/*
 *  タイマ関連
 */
#define CMSTR     UINT_C(0xfffec000)
#define CMCSR_0   UINT_C(0xfffec002)
#define CMCNT_0   UINT_C(0xfffec004)
#define CMCOR_0   UINT_C(0xfffec006)
#define CMCSR_1   UINT_C(0xfffec008)
#define CMCNT_1   UINT_C(0xfffec00A)
#define CMCOR_1   UINT_C(0xfffec00C)

#define CMSTR_STR0   UINT_C(0x0001)
#define CMSTR_STR1   UINT_C(0x0002)
#define CMCSR_CMF    UINT_C(0x0080)
#define CMCSR_CMIE   UINT_C(0x0040)

/*
 *  FIFO付きシリアルコミュニケーションインターフェース(SCIF)レジスタ
 */
#define SH_SCIF0_BASE   UINT_C(0xfffe8000)
#define SH_SCIF1_BASE   UINT_C(0xfffe8800)
#define SH_SCIF2_BASE   UINT_C(0xfffe9000)
#define SH_SCIF3_BASE   UINT_C(0xfffe9800)

/*
 *  ピンファンクションコントローラ
 */
#define PBIORL   UINT_C(0xfffe3886)
#define PBCRL4   UINT_C(0xfffe3890)
#define PBCRL3   UINT_C(0xfffe3892)
#define PBCRL2   UINT_C(0xfffe3894)
#define PBCRL1   UINT_C(0xfffe3896)

#define PCIORL   UINT_C(0xfffe3906)
#define PCCRL4   UINT_C(0xfffe3910)
#define PCCRL3   UINT_C(0xfffe3912)
#define PCCRL2   UINT_C(0xfffe3914)
#define PCCRL1   UINT_C(0xfffe3916)

#define PDIORL   UINT_C(0xfffe3986)
#define PDCRL4   UINT_C(0xfffe3990)
#define PDCRL3   UINT_C(0xfffe3992)
#define PDCRL2   UINT_C(0xfffe3994)
#define PDCRL1   UINT_C(0xfffe3996)

#define PEIORL   UINT_C(0xfffe3a06)
#define PECRL4   UINT_C(0xfffe3a10)
#define PECRL3   UINT_C(0xfffe3a12)
#define PECRL2   UINT_C(0xfffe3a14)
#define PECRL1   UINT_C(0xfffe3a16)

#endif /* TOPPERS_SH7263_H */
