/*
 *  TOPPERS/ASP Kernel
 *      Toyohashi Open Platform for Embedded Real-Time Systems/
 *      Advanced Standard Profile Kernel
 * 
 *  Copyright (C) 2007 by Embedded and Real-Time Systems Laboratory
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2001-2008 by Industrial Technology Institute,
 *                              Miyagi Prefectural Government, JAPAN
 *  Copyright (C) 2008 by Witz Corporation, JAPAN
 * 
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 * 
 */

/*
 *		プロセッサ依存モジュール（SH7263用）
 *
 *  このインクルードファイルは，target_config.h（または，そこからインク
 *  ルードされるファイル）のみからインクルードされる．他のファイルから
 *  直接インクルードしてはならない．
 */

#ifndef TOPPERS_SH7263_CONFIG_H
#define TOPPERS_SH7263_CONFIG_H

/*
 *  バンクレジスタを使用するか
 */

#define USE_BANKED_REG


/*
 * 割込みハンドラ番号に関する定義
 */ 
#define TMIN_INHNO 64U
#define TMAX_INHNO 247U
#define TNUM_INH   (247U - 64U + 1U)

/*
 * 割込み番号に関する定義
 */ 
#define TMIN_INTNO 64U
#define TMAX_INTNO 247U
#define TNUM_INT   (247U - 64U + 1U)

/*
 *  割込み要求ライン毎の優先度設定レジスタの設定情報
 */
#define IPR_INFO_TBL_DATA   \
    {IPR01, 12U}, /*  64 IRQ0 */ \
    {IPR01,  8U}, /*  65 IRQ1 */ \
    {IPR01,  4U}, /*  66 IRQ2 */ \
    {IPR01,  0U}, /*  67 IRQ3 */ \
    {IPR02, 12U}, /*  68 IRQ4 */ \
    {IPR02,  8U}, /*  69 IRQ5 */ \
    {IPR02,  4U}, /*  70 IRQ6 */ \
    {IPR02,  0U}, /*  71 IRQ7 */ \
\
    {0U, 0U},     /*  72 */ \
    {0U, 0U},     /*  73 */ \
    {0U, 0U},     /*  74 */ \
    {0U, 0U},     /*  75 */ \
    {0U, 0U},     /*  76 */ \
    {0U, 0U},     /*  77 */ \
    {0U, 0U},     /*  78 */ \
    {0U, 0U},     /*  79 */ \
\
    {IPR05, 12U}, /*  80 PINT0 */ \
    {IPR05, 12U}, /*  81 PINT1 */ \
    {IPR05, 12U}, /*  82 PINT2 */ \
    {IPR05, 12U}, /*  83 PINT3 */ \
    {IPR05, 12U}, /*  84 PINT4 */ \
    {IPR05, 12U}, /*  85 PINT5 */ \
    {IPR05, 12U}, /*  86 PINT6 */ \
    {IPR05, 12U}, /*  87 PINT7 */ \
\
    {0U, 0U},     /*  88 */ \
    {0U, 0U},     /*  89 */ \
    {0U, 0U},     /*  90 */ \
    {0U, 0U},     /*  91 */ \
    {0U, 0U},     /*  92 */ \
    {0U, 0U},     /*  93 */ \
    {0U, 0U},     /*  94 */ \
    {0U, 0U},     /*  95 */ \
    {0U, 0U},     /*  96 */ \
    {0U, 0U},     /*  97 */ \
    {0U, 0U},     /*  98 */ \
    {0U, 0U},     /*  99 */ \
    {0U, 0U},     /* 100 */ \
    {0U, 0U},     /* 101 */ \
    {0U, 0U},     /* 102 */ \
    {0U, 0U},     /* 103 */ \
    {0U, 0U},     /* 104 */ \
    {0U, 0U},     /* 105 */ \
    {0U, 0U},     /* 106 */ \
    {0U, 0U},     /* 107 */ \
\
    {IPR06, 12U}, /* 108 DMAC0_TEI */ \
    {IPR06, 12U}, /* 109 DMAC0_HEI */ \
\
    {0U, 0U},     /* 110 */ \
    {0U, 0U},     /* 111 */ \
\
    {IPR06,  8U}, /* 112 DMAC1_TEI */ \
    {IPR06,  8U}, /* 113 DMAC1_HEI */ \
\
    {0U, 0U},     /* 114 */ \
    {0U, 0U},     /* 115 */ \
\
    {IPR06,  4U}, /* 116 DMAC2_TEI */ \
    {IPR06,  4U}, /* 117 DMAC2_HEI */ \
\
    {0U, 0U},     /* 118 */ \
    {0U, 0U},     /* 119 */ \
\
    {IPR06,  0U}, /* 120 DMAC3_TEI */ \
    {IPR06,  0U}, /* 121 DMAC3_HEI */ \
\
    {0U, 0U},     /* 122 */ \
    {0U, 0U},     /* 123 */ \
\
    {IPR07, 12U}, /* 124 DMAC4_TEI */ \
    {IPR07, 12U}, /* 125 DMAC4_HEI */ \
\
    {0U, 0U},     /* 126 */ \
    {0U, 0U},     /* 127 */ \
\
    {IPR07,  8U}, /* 128 DMAC5_TEI */ \
    {IPR07,  8U}, /* 129 DMAC5_HEI */ \
\
    {0U, 0U},     /* 130 */ \
    {0U, 0U},     /* 131 */ \
\
    {IPR07,  4U}, /* 132 DMAC6_TEI */ \
    {IPR07,  4U}, /* 133 DMAC6_HEI */ \
\
    {0U, 0U},     /* 134 */ \
    {0U, 0U},     /* 135 */ \
\
    {IPR07,  0U}, /* 136 DMAC7_TEI */ \
    {IPR07,  0U}, /* 137 DMAC7_HEI */ \
\
    {0U, 0U},     /* 138 */ \
    {0U, 0U},     /* 139 */ \
\
    {IPR08, 12U},  /* 140 USB */ \
    {IPR08,  8U},  /* 141 LCDC */ \
\
    {IPR08,  4U},  /* 142 CMI0 */ \
    {IPR08,  0U},  /* 143 CMI1 */ \
\
    {IPR09, 12U},  /* 144 CMI */ \
    {IPR09,  8U},  /* 145 ITI */ \
\
    {IPR09,  4U},  /* 146 MTU0_TGIA */ \
    {IPR09,  4U},  /* 147 MTU0_TGIB */ \
    {IPR09,  4U},  /* 148 MTU0_TGIC */ \
    {IPR09,  4U},  /* 149 MTU0_TGID */ \
    {IPR09,  0U},  /* 150 MTU0_TGIV */ \
    {IPR09,  0U},  /* 151 MTU0_TGIE */ \
    {IPR09,  0U},  /* 152 MTU0_TGIF */ \
\
    {IPR10, 12U},  /* 153 MTU1_TGIA */ \
    {IPR10, 12U},  /* 154 MTU1_TGIB */ \
    {IPR10,  8U},  /* 155 MTU1_TGIV */ \
    {IPR10,  8U},  /* 156 MTU1_TGIU */ \
\
    {IPR10,  4U},  /* 157 MTU2_TGIA */ \
    {IPR10,  4U},  /* 158 MTU2_TGIB */ \
    {IPR10,  0U},  /* 159 MTU2_TGIV */ \
    {IPR10,  0U},  /* 160 MTU2_TGIU */ \
\
    {IPR11, 12U},  /* 161 MTU3_TGIA */ \
    {IPR11, 12U},  /* 162 MTU3_TGIB */ \
    {IPR11, 12U},  /* 163 MTU3_TGIC */ \
    {IPR11, 12U},  /* 164 MTU3_TGID */ \
    {IPR11,  8U},  /* 165 MTU3_TGIV */ \
\
    {IPR11,  4U},  /* 166 MTU4_TGIA */ \
    {IPR11,  4U},  /* 167 MTU4_TGIB */ \
    {IPR11,  4U},  /* 168 MTU4_TGIC */ \
    {IPR11,  4U},  /* 169 MTU4_TGID */ \
    {IPR11,  0U},  /* 170 MTU4_TGIV */ \
\
    {IPR12, 12U},  /* 171 ADI */ \
\
    {IPR12,  8U},  /* 172 IIC3_STPI0 */ \
    {IPR12,  8U},  /* 173 IIC3_NAKI0 */ \
    {IPR12,  8U},  /* 174 IIC3_RXI0 */ \
    {IPR12,  8U},  /* 175 IIC3_TXI0 */ \
    {IPR12,  8U},  /* 176 IIC3_TEI0 */ \
\
    {IPR12,  4U},  /* 177 IIC3_STPI1 */ \
    {IPR12,  4U},  /* 178 IIC3_NAKI1 */ \
    {IPR12,  4U},  /* 179 IIC3_RXI1 */ \
    {IPR12,  4U},  /* 180 IIC3_TXI1 */ \
    {IPR12,  4U},  /* 181 IIC3_TEI1 */ \
\
    {IPR12,  0U},  /* 182 IIC3_STPI2 */ \
    {IPR12,  0U},  /* 183 IIC3_NAKI2 */ \
    {IPR12,  0U},  /* 184 IIC3_RXI2 */ \
    {IPR12,  0U},  /* 185 IIC3_TXI2 */ \
    {IPR12,  0U},  /* 186 IIC3_TEI2 */ \
\
    {IPR13, 12U},  /* 187 IIC3_STPI3 */ \
    {IPR13, 12U},  /* 188 IIC3_NAKI3 */ \
    {IPR13, 12U},  /* 189 IIC3_RXI3 */ \
    {IPR13, 12U},  /* 190 IIC3_TXI3 */ \
    {IPR13, 12U},  /* 191 IIC3_TEI3 */ \
\
    {IPR13,  8U},  /* 192 SCIF0_BRI */ \
    {IPR13,  8U},  /* 193 SCIF0_ERI */ \
    {IPR13,  8U},  /* 194 SCIF0_RXI */ \
    {IPR13,  8U},  /* 195 SCIF0_TXI */ \
\
    {IPR13,  4U},  /* 196 SCIF1_BRI */ \
    {IPR13,  4U},  /* 197 SCIF1_ERI */ \
    {IPR13,  4U},  /* 198 SCIF1_RXI */ \
    {IPR13,  4U},  /* 199 SCIF1_TXI */ \
\
    {IPR13,  0U},  /* 200 SCIF2_BRI */ \
    {IPR13,  0U},  /* 201 SCIF2_ERI */ \
    {IPR13,  0U},  /* 202 SCIF2_RXI */ \
    {IPR13,  0U},  /* 203 SCIF2_TXI */ \
\
    {IPR14, 12U},  /* 204 SCIF3_BRI */ \
    {IPR14, 12U},  /* 205 SCIF3_ERI */ \
    {IPR14, 12U},  /* 206 SCIF3_RXI */ \
    {IPR14, 12U},  /* 207 SCIF3_TXI */ \
\
    {IPR14,  8U},  /* 208 SSU0_SSERI */ \
    {IPR14,  8U},  /* 209 SSU0_SSRXI */ \
    {IPR14,  8U},  /* 210 SSU0_SSTXI */ \
\
    {IPR14,  4U},  /* 211 SSU0_SSERI */ \
    {IPR14,  4U},  /* 212 SSU0_SSRXI */ \
    {IPR14,  4U},  /* 213 SSU0_SSTXI */ \
\
    {IPR14,  0U},  /* 214 SSI0_SSII */ \
    {IPR15, 12U},  /* 215 SSI1_SSII */ \
    {IPR15,  8U},  /* 216 SSI2_SSII */ \
    {IPR15,  4U},  /* 217 SSI3_SSII */ \
\
    {IPR15,  0U},  /* 218 ROM_DEC_ISY */ \
    {IPR15,  0U},  /* 219 ROM_DEC_IERR */ \
    {IPR15,  0U},  /* 220 ROM_DEC_IARG */ \
    {IPR15,  0U},  /* 221 ROM_DEC_ISEC */ \
    {IPR15,  0U},  /* 222 ROM_DEC_IBUF */ \
    {IPR15,  0U},  /* 223 ROM_DEC_IREADY */ \
\
    {IPR16, 12U},  /* 224 FLCTL_FLSTEI */ \
    {IPR16, 12U},  /* 225 FLCTL_FLTENDI */ \
    {IPR16, 12U},  /* 226 FLCTL_FLTREQ0I */ \
    {IPR16, 12U},  /* 227 FLCTL_FLTREQ1I */ \
\
    {IPR16,  8U},  /* 228 SDHI_SDHI3 */ \
    {IPR16,  8U},  /* 229 SDHI_SDHI0 */ \
    {IPR16,  8U},  /* 230 SDHI_SDHI1 */ \
\
    {IPR16,  4U},  /* 231 RTC_ARM */ \
    {IPR16,  4U},  /* 232 RTC_PRD */ \
    {IPR16,  4U},  /* 233 RTC_CUP */ \
\
    {IPR16,  0U},  /* 234 RCAN_TL1_RCAN0_ERS */ \
    {IPR16,  0U},  /* 235 RCAN_TL1_RCAN0_ERS */ \
    {IPR16,  0U},  /* 236 RCAN_TL1_RCAN0_ERS */ \
    {IPR16,  0U},  /* 237 RCAN_TL1_RCAN0_ERS */ \
    {IPR16,  0U},  /* 238 RCAN_TL1_RCAN0_ERS */ \
\
    {IPR17, 12U},  /* 239 RCAN_TL1_RCAN1_ERS */ \
    {IPR17, 12U},  /* 240 RCAN_TL1_RCAN1_ERS */ \
    {IPR17, 12U},  /* 241 RCAN_TL1_RCAN1_ERS */ \
    {IPR17, 12U},  /* 242 RCAN_TL1_RCAN1_ERS */ \
    {IPR17, 12U},  /* 243 RCAN_TL1_RCAN1_ERS */ \
\
    {IPR17,  8U},  /* 244 SRC_OVF */ \
    {IPR17,  8U},  /* 245 SRC_ODFI */ \
    {IPR17,  8U},  /* 246 SRC_IDEI */ \
\
    {IPR17,  4U},  /* 247 IEB_IEBI */


#ifndef TOPPERS_MACRO_ONLY
#pragma inline (init_irc)
/*
 * IRCの初期化
 */
Inline void
init_irc(void)
{
    sil_wrh_mem((void*)ICR0, 0x0000U);
    sil_wrh_mem((void*)ICR1, 0x0000U);
    sil_wrh_mem((void*)ICR2, 0x0000U);
    
#ifdef USE_BANKED_REG
    /* レジスタバンク使用，オーバーフロー例外の禁止 */    
    sil_wrh_mem((void*)IBCR, 0xfffeU);
    sil_wrh_mem((void*)IBNR, 0x4000U);
#else
    /* レジスタバンク使用禁止 */    
    sil_wrh_mem((void*)IBCR, 0x0000U);
    sil_wrh_mem((void*)IBNR, 0x0000U);
#endif /* USE_BANKED_REG */    

    sil_wrh_mem((void*)IPR01, 0x0000U);
    sil_wrh_mem((void*)IPR02, 0x0000U);
    sil_wrh_mem((void*)IPR05, 0x0000U);
    sil_wrh_mem((void*)IPR06, 0x0000U);
    sil_wrh_mem((void*)IPR07, 0x0000U);
    sil_wrh_mem((void*)IPR08, 0x0000U);
    sil_wrh_mem((void*)IPR09, 0x0000U);
    sil_wrh_mem((void*)IPR10, 0x0000U);
    sil_wrh_mem((void*)IPR11, 0x0000U);
    sil_wrh_mem((void*)IPR12, 0x0000U);
    sil_wrh_mem((void*)IPR13, 0x0000U);
    sil_wrh_mem((void*)IPR14, 0x0000U);
    sil_wrh_mem((void*)IPR15, 0x0000U);
    sil_wrh_mem((void*)IPR16, 0x0000U);
    sil_wrh_mem((void*)IPR17, 0x0000U);
}

#endif /* TOPPERS_MACRO_ONLY */


/*
 *  プロセッサ依存モジュール（SH2A用）
 */
#include "sh12a_hew\sh2a_config.h"

#endif /* TOPPERS_SH7263_CONFIG_H */
