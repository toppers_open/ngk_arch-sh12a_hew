@echo off
rem コンフィギュレーション pass1を実行

rem 引数
rem 　%1 aspのトップディレクトリ
rem 　%2 対象となるコンフィギュレーションファイル
rem 　%3 ターゲット名

@echo on
if not exist %1\cfg\cfg\Release\cfg.exe goto CFG_NOT_EXIST
%1\cfg\cfg\Release\cfg.exe --pass=1 --kernel=asp -I. -I%1\include -I%1\arch -I%1  -I%1\target\%3 -I%1\syssvc --api-table %1\kernel\kernel_api.csv --cfg1-def-table %1\kernel\kernel_def.csv %2
if errorlevel 1 goto ERROR_LINE

@echo off
rem   プロジェクト2_cfg_pass2に引数を渡す。（特にコンフィギュレーションファイル名）

echo call %1\arch\sh12a_hew\prc_pass2.bat %1 %2 %3 > call_pass2.bat

rem  バナー表示のタイムスタンプを更新するため、UNIXのtouchコマンド相当の処理を行う。
@echo on
copy %1\syssvc\banner.c tmp.tmp
type tmp.tmp >  %1\syssvc\banner.c
del tmp.tmp
exit


:ERROR_LINE
@echo off
echo コンフィギュレーションのphase1でエラーが発生しました。
echo HEWのビルドメニューから「ツールの中止」を行って下さい。

rem HEWのビルドが先に進まないための無限ループ
:LOOP_LINE
goto LOOP_LINE

:CFG_NOT_EXIST
@echo off
echo コンフィギュレーションのphase1でエラーが発生しました。
echo コンフィギュレータが存在しません。
echo 次のファイルをビルドして下さい。：%1\cfg\cfg\Release\cfg.exe
echo HEWのビルドメニューから「ツールの中止」を行って下さい。
goto LOOP_LINE

