/*
 *	TOPPERS Software
 *		Toyohashi Open Platform for Embedded Real-Time Systems
 *	
 *	Copyright (C) 2007 by Embedded and Real-Time Systems Laboratory
 *				Graduate School of Information Science, Nagoya Univ., JAPAN
 *	Copyright (C) 2007-2009 by Industrial Technology Institute,
 *								Miyagi Prefectural Government, JAPAN
 *	
 *	上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *	ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *	変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *	(1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *		権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *		スコード中に含まれていること．
 *	(2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *		用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *		者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *		の無保証規定を掲載すること．
 *	(3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *		用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *		と．
 *	  (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *		  作権表示，この利用条件および下記の無保証規定を掲載すること．
 *	  (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *		  報告すること．
 *	(4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *		害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *		また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *		由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *		免責すること．
 *	
 *	本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *	よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *	に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *	アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *	の責任を負わない．
 *	
 *	$Id: prc_sil.h 1824 2010-07-02 06:50:03Z mit-kimai $
 */

/*
 *	sil.hのプロセッサ依存部（SH12A用）
 */

#ifndef TOPPERS_PRC_SIL_H
#define TOPPERS_PRC_SIL_H

#ifndef TOPPERS_MACRO_ONLY

#include <machine.h>	/*  処理系の組み込み関数  */

/*
 *	NMIを除くすべての割込みの禁止
 */
Inline uint32_t
TOPPERS_disint(void)
{
    uint32_t  TOPPERS_sr, TOPPERS_sr2;
    
    TOPPERS_sr = (uint32_t)get_cr();
    TOPPERS_sr2 = TOPPERS_sr | 0x000000f0U;
    set_cr((int)TOPPERS_sr2);
    TOPPERS_sr &= 0x000000f0U;
    return(TOPPERS_sr);
}

/*
 *	割込み優先度マスク（内部表現）の現在値の設定
 */
Inline void
TOPPERS_set_iipm(uint32_t TOPPERS_iipm)
{
	uint32_t  TOPPERS_sr;

	TOPPERS_sr = (uint32_t)get_cr();
	TOPPERS_sr = (TOPPERS_sr & ~TOPPERS_SH_SR_I_BIT) | TOPPERS_iipm;
	set_cr((int)TOPPERS_sr);
}

/*
 *	全割込みロック状態の制御
 */
#define SIL_PRE_LOC 	 uint32_t TOPPERS_iipm
#define SIL_LOC_INT()	 ((void)(TOPPERS_iipm = TOPPERS_disint()))
#define SIL_UNL_INT()	 (TOPPERS_set_iipm(TOPPERS_iipm))

/*
 *  エンディアンの反転
 */

/*
 *  依存部のインライン関数
 */
#pragma inline (TOPPERS_disint, TOPPERS_set_iipm)

/*
 *  非依存部のインライン関数
 *  　target_sil.hでエンディアンを定義した後に、
 *  　このファイルがインクルードされる。
 */
#pragma inline (sil_reb_mem, sil_wrb_mem)

#pragma inline (sil_reh_mem, sil_wrh_mem)
#ifdef SIL_ENDIAN_BIG			/* ビッグエンディアンプロセッサ */
#pragma inline (sil_reh_lem, sil_wrh_lem)
#else /* SIL_ENDIAN_BIG */		/* リトルエンディアンプロセッサ */
#pragma inline (sil_reh_bem, sil_wrh_bem)
#endif /* SIL_ENDIAN_BIG */

#pragma inline (sil_rew_mem, sil_wrw_mem)
#ifdef SIL_ENDIAN_BIG			/* ビッグエンディアンプロセッサ */
#pragma inline (sil_rew_lem, sil_wrw_lem)
#else /* SIL_ENDIAN_BIG */		/* リトルエンディアンプロセッサ */
#pragma inline (sil_rew_bem, sil_wrw_bem)
#endif /* SIL_ENDIAN_BIG */


#endif /* TOPPERS_MACRO_ONLY */

#endif /* TOPPERS_PRC_SIL_H */
