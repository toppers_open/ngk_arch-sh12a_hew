@echo off
rem コンフィギュレーション pass2を実行
rem  　プロジェクト1_cfg_pass1で生成したcall_pass2.batから呼び出される。
rem  　（コンフィギュレーションファイル名を引き継ぐため）

rem 引数
rem 　%1 aspのトップディレクトリ
rem 　%2 対象となるコンフィギュレーションファイル
rem 　%3 ターゲット名

@echo on

if exist 2_cfg_pass2.mot (
move 2_cfg_pass2.mot cfg1_out.srec
) else (
move 1st\2_cfg_pass2.mot cfg1_out.srec
)
if errorlevel 1 goto ERROR_COMMON

@echo off
rem シンボルリスト生成
@echo on
if exist 2_cfg_pass2.map (
%1\arch\shc\shc_map_nm.exe 2_cfg_pass2.map
) else (
%1\arch\shc\shc_map_nm.exe 1st\2_cfg_pass2.map
)
if errorlevel 1 goto ERROR_COMMON

%1\cfg\cfg\Release\cfg.exe --pass=2 --kernel=asp -I. -I%1\include -I%1\arch -I%1  -I%1\target\%3 -I%1\syssvc  --api-table %1\kernel\kernel_api.csv --cfg1-def-table %1\kernel\kernel_def.csv -T %1\target\%3\target.tf %2
if errorlevel 1 goto ERROR_CFG

move kernel_cfg.c ..
if errorlevel 1 goto ERROR_COMMON

move kernel_cfg.h ..
if errorlevel 1 goto ERROR_COMMON

move kernel_cfg_asm.src ..
if errorlevel 1 goto ERROR_COMMON

exit

:ERROR_CFG
@echo off
echo コンフィギュレーションのphase2でエラーが発生しました。

:ERROR_COMMON
@echo off
echo HEWのビルドメニューから「ツールの中止」を行って下さい。

rem HEWのビルドが先に進まないための無限ループ
:LOOP_LINE
goto LOOP_LINE
