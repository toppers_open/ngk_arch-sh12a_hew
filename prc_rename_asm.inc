;
;  TOPPERS/ASP Kernel
;      Toyohashi Open Platform for Embedded Real-Time Systems/
;      Advanced Standard Profile Kernel
; 
;  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
;                              Toyohashi Univ. of Technology, JAPAN
;  Copyright (C) 2005,2007 by Embedded and Real-Time Systems Laboratory
;              Graduate School of Information Science, Nagoya Univ., JAPAN
;  Copyright (C) 2001-2008 by Industrial Technology Institute,
;                              Miyagi Prefectural Government, JAPAN
; 
;  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
;  によって公表されている GNU General Public License の Version 2 に記
;  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
;  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
;  利用と呼ぶ）することを無償で許諾する．
;  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
;      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
;      スコード中に含まれていること．
;  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
;      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
;      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
;      の無保証規定を掲載すること．
;  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
;      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
;      と．
;    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
;        作権表示，この利用条件および下記の無保証規定を掲載すること．
;    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
;        報告すること．
;  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
;      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
;
;  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
;  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
;  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
;  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
;
;  @(#) $Id: prc_rename_asm.inc,v 1.6 2005/11/13 14:05:01 honda Exp $
;

 .AIFDEF _PRC_RENAME_ASM_H_
 .AELSE

_PRC_RENAME_ASM_H_: .DEFINE ""


;
;　シンボルのリネーム
;

;
;　共通部
;

;　　　変数
_p_runtsk:              .DEFINE         "__kernel_p_runtsk"
_p_schedtsk:            .DEFINE         "__kernel_p_schedtsk"
_reqflg:                .DEFINE         "__kernel_reqflg"
_dspflg:              	.DEFINE         "__kernel_dspflg"
_istkpt:              	.DEFINE         "__kernel_istkpt"

;　　　関数
_call_texrtn:           .DEFINE         "__kernel_call_texrtn"
_exit_kernel:           .DEFINE         "__kernel_exit_kernel"
_default_int_handler:	.DEFINE      	"__kernel_default_int_handler"
_default_exc_handler:	.DEFINE      	"__kernel_default_exc_handler"

;
;　prc_config.c
;

;　　　変数
_excnest_count:         .DEFINE         "__kernel_excnest_count"
_lock_flag:           	.DEFINE         "__kernel_lock_flag"
_saved_iipm:          	.DEFINE         "__kernel_saved_iipm"


;
;　prc_support.src
;

;　　　関数
_dispatch:              .DEFINE         "__kernel_dispatch"
_start_dispatch:        .DEFINE         "__kernel_start_dispatch"
_exit_and_dispatch:     .DEFINE         "__kernel_exit_and_dispatch"
_start_r:            	.DEFINE        	"__kernel_start_r"
_call_exit_kernel:      .DEFINE        	"__kernel_call_exit_kernel"
_int_entry:				.DEFINE     	"__kernel_int_entry"
_exc_entry:				.DEFINE     	"__kernel_exc_entry"
_default_int_handler_entry: .DEFINE     "__kernel_default_int_handler_entry"

;　　ログトレース
_log_dsp_enter:			.DEFINE       	"__kernel_log_dsp_enter"
_log_dsp_leave:			.DEFINE       	"__kernel_log_dsp_leave"
_log_inh_enter:			.DEFINE       	"__kernel_log_inh_enter"
_log_inh_leave:			.DEFINE       	"__kernel_log_inh_leave"
_log_exc_enter:			.DEFINE       	"__kernel_log_exc_enter"
_log_exc_leave:			.DEFINE       	"__kernel_log_exc_leave"

;
;　start.src
;
start:                  .DEFINE         "__kernel_start"
start_1:                .DEFINE         "__kernel_start_1"

 .AENDI ;  _PRC_RENAME_ASM_H_
